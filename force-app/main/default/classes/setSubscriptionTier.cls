public  class setSubscriptionTier {
    public static subscription__c setTier(subscription__c subs, string tier,list<product2> listProd) {
        //list<product2> listProd = new list<product2>();
        //listProd = [SELECT id,name from product2 where name IN ('Bronze','Gold','Silver') order by name] ;
        if(tier == 'bronze')
            subs.Product__c = listProd[0].id;
        if(tier == 'gold')
            subs.Product__c = listProd[1].id;
        if(tier == 'silver')
            subs.Product__c = listProd[2].id;
        return subs;
    }
}
