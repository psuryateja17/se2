/********************************
 * Created by : Surya Teja
 * Date : 21st April 2020
 * Purpose : The after insert handler is responsible for creating appropriate Person Account 
 *           and subscription reocords. It is also then responsible for checking the Processed__c
 *           checkbox on the Partner_Staging__c object.
 * Version : 1.0
 *******************************/
public  class TriggerHandler_PartnerStaging {
    public static void handleBeforeInsert(map<Id,PartnerStaging__c> newMap) {
        //Do the before insert stuff here//
    }
    public static void handleAfterInsert(map<Id,PartnerStaging__c> newMap,map<Id,PartnerStaging__c> oldMap) {
        try{
            //Do the after insert stuff here//
            //Create lists of Person accounts and Subscriptions that we need to create. 
            list<account> listperson_accounts = new list<account>();
        
            list<Subscription__c> listsubscription = new list<Subscription__c>();
            string personAccRCtypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
            //Let's bulkify here..
            list<product2> listProd = new list<product2>();
            map<string,product2> mapOfProd = new map<string,product2>();
            for(product2 p : [SELECT id,name from product2 order by name]){
                mapOfProd.put(p.name.toLowercase(), p);
            }
            listProd = [SELECT id,name from product2  order by name] ;
            for(PartnerStaging__c ps : newMap.values()){
                //Instance a person account and a subscription
                account personacc = new account();
                personacc.Partner_Staging__c= ps.Id;            //Associate the Person account with PartnerStaging__c this will help us keep track of where the records originated from.
                personacc.Address_line_1__c = ps.Address_Line_1__c;
                personacc.Address_line_2__c = ps.Address_Line_2__c;
                personacc.Address_line_3__c = ps.Address_Line_3__c;
                personacc.PersonMailingCountry = ps.country__c;
                personacc.PersonMailingPostalCode = ps.Post_Code__c;
                personacc.PersonMailingState = ps.County_State__c;
                personacc.firstname = ps.Customer_First_Name__c ;
                personacc.lastname =  ps.Customer_Last_Name__c;
                personacc.recordtypeId = personAccRCtypeID;
                listperson_accounts.add(personacc);

                //Now the Subscription object
                Subscription__c subs = new Subscription__c();
                subs.Subscription_Value__c = ps.Subscription_Value__c;
                subs.Subscription_Type__c = ps.Subscription_Type__c;
                subs.CurrencyIsoCode = ps.CurrencyIsoCode;
                subs.product__c = mapOfProd.get(ps.Subscription_tier__c).Id;
                listsubscription.add(subs);
            }

            //Now lets save the person accounts to get the ID's so that we can link up the subscriptions
            //Before we do that , let set a savepoint so that we can roll back just in case there are errors with the subscription object.
            Savepoint sp = Database.setSavepoint();

            Database.SaveResult[] srList = Database.insert(listperson_accounts, false);
            for (Database.SaveResult sr : srList) {
                integer i=0;
                if (sr.isSuccess()) {
                    System.debug('Successfully inserted account. Account ID: ' + sr.getId());
                    listsubscription[i].Person_Account__c = sr.getId();     //link the subscription with the person acc.
                    i++;
                }
                else {
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Account fields that affected this error: ' + err.getFields());
                    }
                }
            }
            Database.SaveResult[] srList2 = Database.insert(listsubscription, false);
            boolean saveErrors = false;
            for (Database.SaveResult sr : srList2) {
                if (sr.isSuccess()) {
                    System.debug('Successfully inserted subscription. subscription ID: ' + sr.getId());
                }
                else {
                    saveErrors = true;
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('subscription fields that affected this error: ' + err.getFields());
                    }
                }
            }
            if(saveErrors)
                Database.rollback(sp);      //if there were any errors in operation, then roll back.
            else if(saveErrors == false){
                list<PartnerStaging__c> listPS = new list<PartnerStaging__c>();
                for(PartnerStaging__c ps : newMap.values()){
                    PartnerStaging__c p = new PartnerStaging__c();
                    p.Id = ps.Id;
                    p.processed__c = true; listPS.add(p);
                }
                database.update(listPS);
            }    
        }
        catch(exception ex){
            system.debug('***There was an error'+ex.getMessage());
        }
    }
}