public  class fetchChartData {
    @AuraEnabled(cacheable=true)
    public static list<AggregateResult> barChartData() {
        list<AggregateResult> listSubs = new list<AggregateResult>();
        listSubs = [SELECT count(Id) cnt, SUM(Subscription_Value__c) total, product__r.name pname from subscription__c group by product__r.name];
        system.debug('>>>>The subs list is :'+listSubs);
        for(AggregateResult ar: listSubs){
            system.debug('>>>Product :'+ar.get('pname'));
            system.debug('>>>Total Value :'+ar.get('total'));
        }
        return listSubs;
    }
    @AuraEnabled(cacheable=true)
    public static map<string,integer> filteredBarChartData(string startDt , string endDt){

        map<string,integer> mapOfRecords = new map<string,integer>();
        for(subscription__c s : [select id, product__r.name from subscription__c 
                                where Created_Date_Value__c >= :date.valueOf(startDt) and Created_Date_Value__c <= :date.valueOf(endDt)]){
                                 if(!mapOfRecords.containsKey(s.product__r.name)) 
                                    mapOfRecords.put(s.product__r.name, 1);
                                 else if (mapOfRecords.containsKey(s.product__r.name)) {
                                    mapOfRecords.put(s.product__r.name, mapOfRecords.get(s.product__r.name) + 1);
                                 }   
                         
        }
        system.debug('>>>Returning '+mapOfRecords);
        return mapOfRecords; 
    }
}
