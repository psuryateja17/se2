public  class SubscriptionVF {
    
string value{set;}
string curr{get;set;}
string tier{get;set;}

    public string getValue() {
        return value;
    }

    public string getCurr(){
        return curr;
    }
    public string getTier(){
        return tier;
    }

    public list<selectOption> getselectOps(){

        list<selectOption> listops = new list<selectOption>();
        listops.add(new selectOption('USD','USD'));
        listops.add(new selectOption('GBP','GBP'));
        return listops;
    }
}
